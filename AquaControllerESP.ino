/**
 * AquaControllerESP.h - Software for AquaController device.
 * Original Copyright (c) 2017 Vadim Teselkin.
 * web: www.pcb.aquagomel.ru
 * mail-to: Dr.Jarold@gmail.com
 * All right reserved.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <WiFiManager.h>
#include <ESP8266SSDP.h>
#include <ESP8266Ping.h>
#include <NTPClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <FS.h>
#include <ESP8266HTTPClient.h>

#define MAX_BUFFER 4000
#define MAX_STATS 24
#define MAX_TEMP_SENSOR 4
#define DELAY_BEFORE_START 10000
#define DELAY_MESSAGE_UPDATE 10000
#define DELAY_TEMP_UPDATE 55000
#define DELAY_TEMP_UPDATE_STATE 3600000
#define DELAY_PH_UPDATE 60000
#define DELAY_PH_UPDATE_STATE 3600000
// 60 minutes
#define DELAY_NTP_UPDATE 21600000
//Maximum number of PH sensors
#define MAX_PH_SENSOR 1
#define MAX_COMMAND 6
#define NUMITEMS 6
#define MAX_TIMERS_PH 1

unsigned long timeIP, lastNTPtime;
unsigned long lastTemptime;
unsigned long lastPHTime;
unsigned long lastTempStatetime;
unsigned long lastPHStateTime;

word UTC3 = 3; //UTC+3
bool NTPState = false;
bool WIFI_ENABLE = false;
bool isWaitResponse = false;
bool isError = false;
bool isSettingsApply = false;

/*  Add Static temperature */
long startTime = 0;
byte tempStat[4][MAX_STATS] = { 0 };
byte phStat[MAX_STATS] = { 0 };

unsigned int localUdpPort = 8888;
char incomingPacket[MAX_BUFFER];
int UTC_ADDR = 1;

//------------------type of message-----------------------
String GET_COMMAND = "get";
String POST_COMMAND = "post";
String INFO_COMMAND = "info";
//----------------------------------------------------------

//----------------------get request ----------------------
String GET_PARAM_REQUEST = "{\"status\":\"get\",\"message\":\"set\"}";
String GET_DEVICE_INFO = "dev";
String GET_DEVICE_CHANAL_SETTINGS = "c_s";
String GET_DEVICE_TEMP_SENSOR = "t_sen";
String GET_DEVICE_TEMP_STATE = "te_s";
String GET_DEVICE_DAILY_TIMER_SATE = "td_s";
String GET_DEVICE_HOURS_TIMER_SATE = "th_s";
String GET_DEVICE_SECOND_TIMER_SATE = "ts_s";

//1.3.7
// PH - constants
String DEVICE_PH_TIMER = "ph_timer";
String GET_DEVICE_PH = "ph_state";

String PH_TIMER_START = "ph_s";
String PH_TIMER_END = "ph_e";
String PH_TIMER_STATE = "ph_st";
String PH_TIMER_CANAL = "ph_c";
String PH_TIMER_401 = "ph_401";
String PH_TIMER_686 = "ph_686";

byte PHTimerStart[MAX_TIMERS_PH] = { 0 };
byte PHTimerEnd[MAX_TIMERS_PH] = { 0 };
byte PHTimerState[MAX_TIMERS_PH] = { 0 };
byte PHTimerCanal[MAX_TIMERS_PH] = { 0 };
byte PHTimer401[MAX_TIMERS_PH] = { 0 };
byte PHTimer686[MAX_TIMERS_PH] = { 0 };

// The lowest possible setting is the PH
const word MIN_PH = 400;
// Maximum possible maximum PH
const word MAX_PH = 1600;
// Minimum PH index: MIN_TEMP + STEP* MIN_INDEX_TEMP
const byte MIN_INDEX_PH = 0;
// The maximum possible PH index: MIN_TEMP + STEP* MAX_INDEX_TEMP
const byte MAX_INDEX_PH = 120;
// Minimal step of PH change
const byte STEP_PH = 10;

byte PhCurrentIndex = 0;
word Ph_6_86_level = 1; //797
word Ph_4_01_level = 1; //844
const word PHLevel = A0;
float Ph6_86 = 6.86f;
float Ph4_01 = 4.01f;

String GET_DEVICE_TEMP_STATS = "temp_stats";

//----------------------------------------------------------

//--------------------post request--------------------------
String CANAL_STATE = "c_s";
String CANAL_TIMER = "c_t";

String TIMER_DAILY_STATE = "td_s";

String DAILY_TIMER_HOUR_START = "dt_h_s";
String DAILY_TIMER_HOUR_END = "dt_h_end";
String DAILY_TIMER_MIN_START = "dt_m_s";
String DAILY_TIMER_MIN_END = "dt_m_e";
String DAILY_TIMER_STATE = "dt_s";
String DAILY_TIMER_CANAL = "dt_c";

String TEMP_STATE = "te_s";

String TEMP_TIMER_STATE = "tt_s";
String TEMP_TIMER_MIN_START = "tt_m_s";
String TEMP_TIMER_MAX_END = "tt_m_e";
String TEMP_TIMER_CHANAL = "tt_c";

String TIMER_HOURS_STATE = "th_s";

String HOURS_TIMER_MIN_START = "ht_m_st";
String HOURS_TIMER_MIN_STOP = "ht_m_sp";
String HOURS_TIMER_STATE = "ht_s";
String HOURS_TIMER_CANAL = "ht_c";

String TIMER_SECONDS_STATE = "ts_s";

String SECOND_TIMER_HOUR_START = "st_h_s";
String SECOND_TIMER_MIN_START = "st_m_s";
String SECOND_TIMER_DURATIONT = "st_d";
String SECOND_TIMER_STATE = "st_s";
String SECOND_TIMER_CANAL = "st_c";

//---------------------------------------------------------------
//type of error
String RESPONSE_NOT_RESPONSE = "Not response";
String RQUEST_NOT_VALID = "Request not valid";
String RQUEST_JSON_CORUPTED = "Request is corrupted";
String RQUEST_DATA_CORUPTED = "Data is corrupted";

//type of response
String RQUEST_COMPLETE = "Request complete";

//settings param
String SETTINGS = "set";
String SETTINGS_SSID = "SSID";
String SETTINGS_PASS = "PASS";
String SETTINGS_NTP = "NTP";
String SETTINGS_AUTO = "AUTO";
String SETTINGS_MAX_CHANALS = "TIMERS";
String SETTINGS_MAX_TEMP_SENSOR = "SENSOR";
String SETTINGS_UTC = "utc";
String VERTION_PROTOCOL = "/4/";
String UPDATE_URL = "http://update.aquacontroller.ru";
const size_t bufferSize = 6 * JSON_ARRAY_SIZE(10) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(6) + 300;
DynamicJsonBuffer jsonBuffer(bufferSize);
//--------------------------JSONs--------------------------------

//dont't change this array, because you need review all code!!!
String requestList[MAX_COMMAND] = { "{\"status\":\"get\",\"message\":\"c_s\"}", "{\"status\":\"get\",\"message\":\"td_s\"}",
		"{\"status\":\"get\",\"message\":\"th_s\"}", "{\"status\":\"get\",\"message\":\"ts_s\"}", "{\"status\":\"get\",\"message\":\"te_s\"}",
		"{\"status\":\"get\",\"message\":\"t_sen\"}" };

//dont't change this array, because you need review all code!!!
String responseCommand[MAX_COMMAND] = { GET_DEVICE_CHANAL_SETTINGS, GET_DEVICE_DAILY_TIMER_SATE, GET_DEVICE_HOURS_TIMER_SATE, GET_DEVICE_SECOND_TIMER_SATE,
		GET_DEVICE_TEMP_STATE, GET_DEVICE_SECOND_TIMER_SATE };

String responseCache[MAX_COMMAND + 1] = { "" };
String responseCacheTemp = "";
String responseCachePH = "";
String responseDevice = "{\"status\":\"success\",\"message\":\"device\",\"data\":{\"ver\":\"AQ_CH08WP\",\"m_t\":10,\"m_t_se\":4,\"min_t\":1600,\"max_t\":3500}}";
String responseNull = "{\"status\":\"error\",\"message\":\"Not Initialized\",\"data\":{}}";
//---------------------------------------------------------------

ESP8266WebServer http(80);
ESP8266HTTPUpdateServer updaterServer;
WiFiUDP Udp; // @suppress("Abstract class cannot be instantiated")
HTTPClient client;

IPAddress broadcastAddress;

String lastCommand = "";
String lastMessage = "";
IPAddress timeServerIP;

byte NTPServerCount = 3;
const char* ntpServerName[] = { "time2.google.com", "time3.google.com", "time4.google.com" };
byte indexNTPServer = 0;

bool isLogEnable = true;
bool isConfigEnable = true;

void setup() {

	pinMode(2, OUTPUT);
	digitalWrite(2, HIGH);
	timeIP = millis();
	lastNTPtime = millis();
	lastTemptime = millis();
	Serial.begin(9600);
	delay(DELAY_BEFORE_START);
	EEPROM.begin(24);
	SPIFFS.begin();
	GetUTC();
	SendWiFiLog("WiFi:v1.3.7-0.5");
	delay(DELAY_BEFORE_START);
	SendWiFiLog("WiFi:Init complete..");
	String response = GetPreferenseFromArduino();
	if (response.indexOf(SETTINGS) != -1) {
		SendWiFiLog("WiFi:Load config...");
		if (GetConfigFromController(response)) {
			if (Connection()) {
				isError = false;
				indexNTPServer = 0;
				updaterServer.setup(&http);
				SendWiFiLog("HTTP:Service started...");
				OtaDownloadWebContent();
				SetSiteMap();
				SetNTPTimeToController();
				StartCaching();
			}
		} else {
			SendWiFiLog("WiFi:Error config...");
		}

	} else {
		SendWiFiLog("Couldn't get config");
		isError = true;
		SendWiFiLog("WiFi:Error...");
	}
}

const IPAddress remote_ip(8, 8, 8, 8);
bool isInterenetAvalible = true;
bool isSerialEvent = false;
bool isEndMessage = false;
byte indexWait = 0;

void loop() {

	if (WIFI_ENABLE && !isError) {
		if (WiFi.status() == WL_CONNECTED) {
			http.handleClient();
			int packetSize = Udp.parsePacket();
			if (packetSize) {
				int len = Udp.read(incomingPacket, MAX_BUFFER);
				if (len > 0) {
					if (len > MAX_BUFFER) {
						len = MAX_BUFFER;
					}
					incomingPacket[len] = 0;
				}
				digitalWrite(2, LOW);
				delay(50);
				digitalWrite(2, HIGH);
				//if isWaitResponse = false then we send cached data
				isWaitResponse = SendFromUDPToController(incomingPacket);
				memset(incomingPacket, 0, sizeof(incomingPacket));
				if (!isWaitResponse) {
					return;
				}
			}
		}
	}
	//wait response from arduino max 2000ms
	indexWait = 0;
	while (Serial.available() <= 0 && isWaitResponse) {
		delay(10);
		indexWait++;
		if (indexWait > 200) {
			break;
		}
	}
	//read from UART
	String response = "";
	if (Serial.available()) {
		delay(10);
		isEndMessage = false;
		while (Serial.available()) {
			char ch = Serial.read();
			switch (ch) {
			case '\n':
			case '\r':
				isEndMessage = true;
				break;
			}
			if (!isEndMessage)
				response += ch;
			delay(1);
		}
		if (isWaitResponse) {
			jsonBuffer.clear();
			JsonObject& root = jsonBuffer.parseObject(response);
			if (!root.success()) {
				UDPSendError(RQUEST_JSON_CORUPTED);
				isWaitResponse = false;
				return;
			}
			//caching and send message for listeners
			StartCachingUART(response);
		} else {

			if (response.indexOf(SETTINGS) != -1 && !isSettingsApply) {
				isSettingsApply = true;
				SendWiFiLog("WiFi:Load config...");
				if (GetConfigFromController(response)) {
					if (Connection())
						isError = false;
					SetNTPTimeToController();
				} else {
					SendWiFiLog("WiFi:Error config...");
					isError = true;
				}
				isSettingsApply = false;
				return;
			} else {
				//caching and send broadcast message for listeners
				if (response.length() > 0) {
					StartCachingUART(response);
				}
			}
		}
	}
	if (isWaitResponse) {
		UDPSendError(incomingPacket);
		isWaitResponse = false;
	}

	if (millis() > timeIP + DELAY_MESSAGE_UPDATE) {
		if (WIFI_ENABLE && !isError) {
			SendWifiIp(false, true);
		} else {
			SendWiFiLog(lastMessage, false);
		}
		timeIP = millis();
		return;
	}

	if (millis() > lastNTPtime + DELAY_NTP_UPDATE) {
		indexNTPServer = 0;
		lastNTPtime = millis();
		if (WIFI_ENABLE && !isError) {
			SetNTPTimeToController();
			return;
		}
	}
	//get temp every DELAY_TEMP_UPDATE time
	if (millis() > lastTemptime + DELAY_TEMP_UPDATE) {
		if (WIFI_ENABLE && !isError) {
			GetTempForController();
			return;
		}
	}
	//get PH every DELAY_PH_UPDATE time
	if (millis() > lastPHTime + DELAY_PH_UPDATE) {
		if (WIFI_ENABLE && !isError) {
			ReadPHValueIndex();
			return;
		}
	}
}

bool SendFromUDPToController(String inString) {
	jsonBuffer.clear();
	JsonObject& root = jsonBuffer.parseObject(inString);
	if (!root.success()) {
		UDPSendError(RQUEST_JSON_CORUPTED);
		return false;
	}

	if (inString.indexOf(GET_COMMAND) != -1) {
		if (inString.indexOf(GET_DEVICE_INFO) != -1) {
			GetDevices();
			return false;
		} else if (inString.indexOf(GET_DEVICE_PH) != -1) {
			UDPSendMessage(responseCachePH, false);
			return false;
		} else if (inString.indexOf(DEVICE_PH_TIMER) != -1) {
			SendPhTimerConfig();
			return false;
		} else if (inString.indexOf(GET_DEVICE_TEMP_STATS) != -1) {
			UDPSendMessage(responseCacheTemp, false);
			return false;
		} else if (inString.indexOf(GET_DEVICE_TEMP_SENSOR) != -1 && IsDataCached(responseCache[5])) {
			UDPSendMessage(responseCache[5], false);
			return false;
		} else if (inString.indexOf(GET_DEVICE_CHANAL_SETTINGS) != -1 && IsDataCached(responseCache[0])) {
			SendToSerial(inString, true);
			return true;
		} else if (inString.indexOf(GET_DEVICE_DAILY_TIMER_SATE) != -1 && IsDataCached(responseCache[1])) {
			SendToSerial(inString, true);
			return true;
		} else if (inString.indexOf(GET_DEVICE_HOURS_TIMER_SATE) != -1 && IsDataCached(responseCache[2])) {
			SendToSerial(inString, true);
			return true;
		} else if (inString.indexOf(GET_DEVICE_SECOND_TIMER_SATE) != -1 && IsDataCached(responseCache[3])) {
			SendToSerial(inString, true);
			return true;
		} else if (inString.indexOf(GET_DEVICE_TEMP_STATE) != -1 && IsDataCached(responseCache[4])) {
			UDPSendMessage(responseCache[4], false);
			return false;
		}

		SendToSerial(inString, true);
		return true;

	} else if (inString.indexOf(POST_COMMAND) != -1) {
		if (inString.indexOf("data") == -1) {
			UDPSendError(RQUEST_DATA_CORUPTED);
			return false;
		}
		JsonObject& data = root["data"];
		if (inString.indexOf("time_NTP") != -1) {
			UTC3 = data[SETTINGS_UTC].as<word>();
			SetUTC();
			SendToSerial(inString, true); //TODO maybe need cut UTC param
			UDPSendMessage("{\"status\":\"success\",\"message\":\"Time update\",\"data\":{}}", false);
			// we don't wait response from arduino when we try set time on device
			return false;
		}
		if (inString.indexOf(CANAL_STATE) != -1) {
			SendPostParam(data, CANAL_STATE, CANAL_TIMER);
			SendToSerial(requestList[0], true);
			return true;
		}
		if (inString.indexOf(TIMER_DAILY_STATE) != -1) {
			SendPostParam(data, TIMER_DAILY_STATE, DAILY_TIMER_HOUR_START);
			SendPostParam(data, TIMER_DAILY_STATE, DAILY_TIMER_HOUR_END);
			SendPostParam(data, TIMER_DAILY_STATE, DAILY_TIMER_MIN_START);
			SendPostParam(data, TIMER_DAILY_STATE, DAILY_TIMER_MIN_END);
			SendPostParam(data, TIMER_DAILY_STATE, DAILY_TIMER_STATE);
			SendPostParam(data, TIMER_DAILY_STATE, DAILY_TIMER_CANAL);
			SendToSerial(requestList[1], true);
			return true;
		}
		if (inString.indexOf(TEMP_STATE) != -1) {
			SendPostParam(data, TEMP_STATE, TEMP_TIMER_STATE);
			SendPostParam(data, TEMP_STATE, TEMP_TIMER_MIN_START);
			SendPostParam(data, TEMP_STATE, TEMP_TIMER_MAX_END);
			SendPostParam(data, TEMP_STATE, TEMP_TIMER_CHANAL);
			SendToSerial(requestList[4], true);
			return true;
		}
		if (inString.indexOf(TIMER_HOURS_STATE) != -1) {
			SendPostParam(data, TIMER_HOURS_STATE, HOURS_TIMER_MIN_START);
			SendPostParam(data, TIMER_HOURS_STATE, HOURS_TIMER_MIN_STOP);
			SendPostParam(data, TIMER_HOURS_STATE, HOURS_TIMER_STATE);
			SendPostParam(data, TIMER_HOURS_STATE, HOURS_TIMER_CANAL);
			SendToSerial(requestList[2], true);
			return true;
		}
		if (inString.indexOf(TIMER_SECONDS_STATE) != -1) {
			SendPostParam(data, TIMER_SECONDS_STATE, SECOND_TIMER_HOUR_START);
			SendPostParam(data, TIMER_SECONDS_STATE, SECOND_TIMER_MIN_START);
			SendPostParam(data, TIMER_SECONDS_STATE, SECOND_TIMER_DURATIONT);
			SendPostParam(data, TIMER_SECONDS_STATE, SECOND_TIMER_STATE);
			SendPostParam(data, TIMER_SECONDS_STATE, SECOND_TIMER_CANAL);
			SendToSerial(requestList[3], true);
			return true;
		}
		if (inString.indexOf(DEVICE_PH_TIMER) != -1) {
			SetJsonValue(PHTimerStart, MAX_TIMERS_PH, PH_TIMER_START, data);
			SetJsonValue(PHTimerEnd, MAX_TIMERS_PH, PH_TIMER_END, data);
			SetJsonValue(PHTimerState, MAX_TIMERS_PH, PH_TIMER_STATE, data);
			SetJsonValue(PHTimerCanal, MAX_TIMERS_PH, PH_TIMER_CANAL, data);
			if (data[PH_TIMER_401].as<byte>() == 0) {
				Set_PH_TIMER_401();
			}
			if (data[PH_TIMER_686].as<byte>() == 0) {
				Set_PH_TIMER_686();
			}
			SendPhTimerConfig();
			return false;

		}

	} else if (inString.indexOf(INFO_COMMAND) != -1) {
		SendToSerial(inString, true);
		UDPSendError(RQUEST_COMPLETE);
		// we don't wait response from arduino when we try sent info
		return false;
	}

	return false;
}
bool IsDataCached(String response) {
	if (response.length() == 0)
		return false;
	jsonBuffer.clear();
	JsonObject& root = jsonBuffer.parseObject(response);
	if (!root.success()) {
		return false;
	}
	return true;
}

void SetJsonValue(byte arrayData[], const byte count, const String key, const JsonObject& root) {
	if (!root.containsKey(key))
		return;
	for (byte i = 0; i < count; i++) {
		arrayData[i] = root[key][i].as<byte>();
	}

}

String GetJsonValue(byte arrayData[], const byte count) {
	String result = "";
	result += ":[";
	for (byte i = 0; i < count; i++) {
		result += String(arrayData[i]);

		if (i != count - 1) {
			result += ",";
		}
	}
	result += "]";
	return result;
}

void SendPostParam(JsonObject& data, String type, String param) {
	JsonArray& array = data[param];
	String requset = "{\"status\":\"post\",\"message\":\"" + type + "\",\"data\":{\"" + param + "\":[";
	for (byte i = 0; i < array.size(); i++) {
		requset += data[param][i].as<byte>();
		if (i != array.size() - 1) {
			requset += (",");
		}
	}
	requset += "]}}";
	SendToSerial(requset, true);
	delay(300);
}

bool Connection() {

	if (WIFI_ENABLE) {
		SendWiFiLog("WiFi:Try connect...");
		WiFiManager wifiManager;

		//Disable debug log connection
		wifiManager.setDebugOutput(false);
		wifiManager.setAPCallback(configModeCallback);
		wifiManager.setSaveConfigCallback(saveConfigCallback);
		//set custom ip for portal
		wifiManager.setAPStaticIPConfig(IPAddress(192, 168, 4, 1), IPAddress(192, 168, 4, 1), IPAddress(255, 255, 255, 0));
		/**
		 * if we couldn't connected to save WiFi access point
		 * we will start our own access point with ip address 192.168.1.4
		 */
		wifiManager.autoConnect("AP: AquaController");
		Udp.begin(localUdpPort);
		broadcastAddress = (uint32_t) WiFi.localIP() | ~((uint32_t) WiFi.subnetMask());
		SendWifiIp(true, true);
		return true;
	} else {
		SendWiFiLog("WiFi:Disable...");
		return false;
	}
}

/**
 * callback for config mode
 */
void configModeCallback(WiFiManager *myWiFiManager) {
	if (WIFI_ENABLE && !isError) {

		SendWiFiLog("WiFi:Failed connect");
		SendWiFiLog("WiFi:Config mode...");
		SendWiFiLog("WiFi:Server start...");
		SendWiFiLog("WiFi:192.168.1.4...");
		Udp.begin(localUdpPort);
		broadcastAddress = (uint32_t) WiFi.localIP() | ~((uint32_t) WiFi.subnetMask());
		SendWifiIp(false, true);
	} else {
		SendWiFiLog("WiFi:Disable...");
	}
}

/**
 * callback for save config
 */
void saveConfigCallback() {

	SendWiFiLog("WiFi:Save config");
	SendWiFiLog("WiFi:Try reconnect..");
}

void SendWifiIp(bool isNeedDelay, bool isNeedPing) {
	if (!isLogEnable || isWaitResponse)
		return;
	if (isNeedPing) {
		isInterenetAvalible = Ping.ping(remote_ip);
	}
	if (!isInterenetAvalible) {
		lastMessage = "LAN:" + WiFi.localIP().toString();
	} else {
		lastMessage = "WAN:" + WiFi.localIP().toString();
	}

	SendWiFiLog(lastMessage, isNeedDelay);

}

/**
 *  "{\"status\":\"success\",\"message\":\"settings\",\"NTP\" : 1,\"AUTO\" : 1}";
 */
String GetPreferenseFromArduino() {

	String response = "";
	if (!isConfigEnable) {
		response = "{\"status\":\"success\",\"message\":\"settings\",\"NTP\" : 1,\"AUTO\" : 1}";
		return response;
	}

	SendToSerial(GET_PARAM_REQUEST, true);
	indexWait = 0;
	while (Serial.available() <= 0) {
		delay(50);
		indexWait++;
		if (indexWait > 250) {
			break;
		}
	}
	indexWait = 0;
	if (Serial.available() > 0) {
		response = Serial.readStringUntil('\n');
		response.trim();
	}
	return response;
}

void SendToSerial(String message, bool isEndMessage) {
	if (isEndMessage)
		Serial.println(message);
	else
		Serial.print(message);
}

void SendWiFiLog(String log) {
	if (!isLogEnable || isWaitResponse)
		return;
	lastMessage = log;
	SendWiFiLog(lastMessage, true);
}

//{"wifi_log":"log"}
void SendWiFiLog(String log, bool isNeedDelay) {
	String logSend = "";
	if (!isLogEnable || isWaitResponse)
		return;
	if (log.length() < 20) {
		for (byte i = log.length(); i < 20; i++) {
			log += " ";
		}
	}
	Serial.print("{\"status\":\"info\",\"message\":\"wifi_log\",\"log\":\"");
	Serial.println(log + "\"}");

	if (isNeedDelay)
		delay(1000);
}

byte attempt = 0;
void GetTempForController() {
	Serial.println("{\"status\":\"get\",\"message\":\"t_sen\"}");
	attempt++;
	auto res = WaitForResponse();
	jsonBuffer.clear();
	JsonObject& root = jsonBuffer.parseObject(res);
	if (!root.success()) {
		GetTempForController();
		if (attempt > 3) {
			attempt = 0;
			return;
		}
	} else {
		attempt = 0;
		if (SetStatTemp(res)) {
			return;
		} else {
			GetTempForController();
		}
	}
}

void GetDevices() {
	timeIP = millis() + DELAY_MESSAGE_UPDATE;
	lastTemptime = millis() + DELAY_MESSAGE_UPDATE;
	lastPHTime = millis() + DELAY_MESSAGE_UPDATE;
	UDPSendMessage(responseDevice, false);
}

/// @return
bool SetNTPTimeToController() {

	if (!WIFI_ENABLE || isError)
		return false;
	if (!NTPState) {
		SendWiFiLog("NTP:Update disable..");
		return false;
	}
	if (!isInterenetAvalible) {
		SendWiFiLog("WAN:Not connection..");
		return false;
	}
	SendWiFiLog("NTP:Sending packet:");

	SendWiFiLog(ntpServerName[indexNTPServer]);
	if (WiFi.hostByName(ntpServerName[indexNTPServer], timeServerIP) != 1) {
		SendWiFiLog("NTP:Lost connection");
		indexNTPServer++;
		if (indexNTPServer < NTPServerCount)
			SetNTPTimeToController();
		return false;
	}
	WiFiUDP UdpNTP; // @suppress("Abstract class cannot be instantiated")
	NTPClient timeClient(UdpNTP, ntpServerName[indexNTPServer]);
	timeClient.begin();
	timeClient.update();
	delay(1000);
	SendToSerial("{\"status\":\"post\",\"message\":\"time_NTP\",\"data\":{",
	false);
	SendToSerial("\"epoch\":", false);
	SendToSerial(String(timeClient.getEpochTime() + UTC3 * 3600), false);
	SendToSerial("}}", true);
	delay(500);
	SendWiFiLog("NTP:Time update");
	timeClient.end();
	return true;

}

/// @param resposeCommand
/// @return result
bool GetConfigFromController(String resposeCommand) {
	jsonBuffer.clear();
	JsonObject& root = jsonBuffer.parseObject(resposeCommand);
	if (root.success()) {
		NTPState = root[SETTINGS_NTP];
		WIFI_ENABLE = root[SETTINGS_AUTO];

		return true;
	}
	return false;
}
//-----------------------------------------UDP----------------------------------------//
/// @param error
void UDPSendError(String error) {
	if (!WIFI_ENABLE) {
		SendWiFiLog("WiFi:Disable...");
		return;
	}
	if (isError) {
		SendWiFiLog("WiFi:Error...");
		return;
	}
	String response = "{\"status\":\"error\",\"message\":\"" + error + "\",\"data\":{}}";
	Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
	Udp.println(response);
	Udp.endPacket();

}

/// @param message
/// @param isBroadcast
void UDPSendMessage(String message, bool isBroadcast) {
	if (!WIFI_ENABLE) {
		SendWiFiLog("WiFi:Disable...");
		return;
	}
	if (isError) {
		SendWiFiLog("WiFi:Error...");
		return;
	}
	if (isBroadcast)
		Udp.beginPacket(broadcastAddress, localUdpPort);
	else
		Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
	Udp.println(message);
	Udp.endPacket();
}

//------------------------------------EEPROM-----------------------------//
void SetUTC() {
	EEPROM.write(UTC_ADDR, UTC3);
	EEPROM.commit();
}

void GetUTC() {
	UTC3 = EEPROM.read(UTC_ADDR);
}
//------------------------------------Statistics Temperature------------------------------------//

bool SetStatTemp(String response) {
	if (response.indexOf(GET_DEVICE_TEMP_SENSOR) == -1) {
		return false;
	}
	jsonBuffer.clear();
	JsonObject& root = jsonBuffer.parseObject(response);
	if (!root.success()) {
		return false;
	}
	if (response.indexOf("data") == -1) {
		return false;
	}
	JsonObject& data = root["data"];
	if (!data.containsKey("t_se"))
		return false;
	responseCache[5] = response;
	lastTemptime = millis();
	UDPSendMessage(response, true);

	if (millis() > lastTempStatetime + DELAY_TEMP_UPDATE_STATE || responseCacheTemp.length() == 0) {
		lastTempStatetime = millis();
		for (byte j = 0; j < MAX_TEMP_SENSOR; j++) {
			for (byte i = 1; i < MAX_STATS; i++) {
				tempStat[j][i - 1] = tempStat[j][i];
			}
		}
		for (byte j = 0; j < MAX_TEMP_SENSOR; j++) {
			tempStat[j][MAX_STATS - 1] = data["t_se"][j].as<byte>(); //rand() % 256;
		}
		responseCacheTemp = "{\"status\":\"success\",\"message\":\"temp_stats\",\"data\":{";
		for (byte j = 0; j < MAX_TEMP_SENSOR; j++) {
			if (j != 0) {
				responseCacheTemp += ",";
			}
			responseCacheTemp += "\"sensor" + String(j) + "\":";
			responseCacheTemp += "[";
			for (byte i = 0; i < MAX_STATS - 1; i++) {
				responseCacheTemp += String(tempStat[j][i]);
				responseCacheTemp += ",";
			}
			responseCacheTemp += String(tempStat[j][MAX_STATS - 1]);
			responseCacheTemp += "]";
		}
		responseCacheTemp += "}}";
		delay(500);
		UDPSendMessage(responseCacheTemp, true);
	}
	return true;
}

/* ------------------PH----------------------*/

void ReadPHValueIndex() {
	word measure = analogRead(PHLevel);
	float V6_86 = 5 / 1024.0 * Ph_6_86_level;
	float V4_01 = 5 / 1024.0 * Ph_4_01_level;
	float PH_step = (V6_86 - V4_01) / (Ph6_86 - Ph4_01);
	float voltage = 5 / 1024.0 * measure;
	float PH_probe = Ph6_86 - ((V6_86 - voltage) / PH_step);
	PhCurrentIndex = ConvertPHWordToByte(PH_probe * 100); //rand() % 256;
	if (millis() > lastPHStateTime + DELAY_PH_UPDATE_STATE || responseCachePH.length() == 0) {
		lastPHStateTime = millis();
		for (byte i = 1; i < MAX_STATS; i++) {
			phStat[i - 1] = phStat[i];
		}
		phStat[MAX_STATS - 1] = PhCurrentIndex;
	}
	responseCachePH = "{\"status\":\"success\",\"message\":\"ph_state\",\"data\":{\"ph\":[" + String(PhCurrentIndex) + "],\"stat\":[";

	for (byte i = 0; i < MAX_STATS - 1; i++) {
		responseCachePH += String(phStat[i]);
		responseCachePH += ",";
	}
	responseCachePH += String(phStat[MAX_STATS - 1]);
	responseCachePH += "]}}";
	lastPHTime = millis();
	UDPSendMessage(responseCachePH, true);

}

byte ConvertPHWordToByte(const word ph) {
	return (ph - MIN_PH) / STEP_PH;
}

void SendPhTimerConfig() {
	String result = "";
	result += "{\"status\":\"success\",\"message\":\"ph_timer\",\"data\":{\"ph_s\"";
	result += GetJsonValue(PHTimerStart, MAX_TIMERS_PH);
	result += ",\"ph_e\"";
	result += GetJsonValue(PHTimerEnd, MAX_TIMERS_PH);
	result += ",\"ph_st\"";
	result += GetJsonValue(PHTimerState, MAX_TIMERS_PH);
	result += ",\"ph_c\"";
	result += GetJsonValue(PHTimerCanal, MAX_TIMERS_PH);
	result += ",\"ph_401\":";
	result += Ph_4_01_level;
	result += ",\"ph_686\":";
	result += Ph_6_86_level;
	result += "}}";
	UDPSendMessage(result, false);
	delay(100);
	UDPSendMessage(result, true);
}

void Set_PH_TIMER_686() {
	word measure = analogRead(PHLevel);
	Ph_6_86_level = measure;
}

void Set_PH_TIMER_401() {
	word measure = analogRead(PHLevel);
	Ph_4_01_level = measure;
}

/*--------------------CACHING----------------------*/

void StartCachingUART(String response) {
	JsonObject& root = jsonBuffer.parseObject(response);
	if (!root.success())
		return;
	if (response.indexOf(GET_DEVICE_CHANAL_SETTINGS) != -1) {
		responseCache[0] = response;
	}
	if (response.indexOf(GET_DEVICE_DAILY_TIMER_SATE) != -1) {
		responseCache[1] = response;
	}
	if (response.indexOf(GET_DEVICE_HOURS_TIMER_SATE) != -1) {
		responseCache[2] = response;
	}
	if (response.indexOf(GET_DEVICE_SECOND_TIMER_SATE) != -1) {
		responseCache[3] = response;
	}
	if (response.indexOf(GET_DEVICE_TEMP_STATE) != -1) {
		responseCache[4] = response;
	}
	if (response.indexOf(GET_DEVICE_TEMP_SENSOR) != -1) {
		responseCache[5] = response;
	}
	UDPSendMessage(response, false);
	delay(100);
	UDPSendMessage(response, true);
	isWaitResponse = false;
}

void StartCaching() {
	SendWiFiLog("WiFi:Caching data...");
	for (int i = 0; i < MAX_COMMAND; i++) {
		SetCacheResult(requestList[i], i);
	}
}

byte attemptCache = 0;
bool SetCacheResult(String request, int index) {
	Serial.println(request);
	delay(100);
	auto res = WaitForResponse();
	jsonBuffer.clear();
	JsonObject& root = jsonBuffer.parseObject(res);
	if (!root.success()) {
		attemptCache++;
		if (attemptCache > 3) {
			attemptCache = 0;
			return false;
		}
		SetCacheResult(request, index);
	}
	responseCache[index] = res;
	return true;
}

String WaitForResponse() {

	indexWait = 0;
	String response = "";
	while (Serial.available() <= 0) {
		delay(10);
		indexWait++;
		if (indexWait > 200) {
			break;
		}
	}
	if (Serial.available()) {
		delay(10);
		isEndMessage = false;
		while (Serial.available()) {
			char ch = Serial.read();
			switch (ch) {
			case '\n':
			case '\r':
				isEndMessage = true;
				break;
			}
			if (!isEndMessage)
				response += ch;
			delay(1);
		}
	}
	return response;
}

/*--------------------WEB----------------------*/

void handleRoot() {
	http.send(200, "text/html", "");
}

void SetSiteMap() {
	//http.on("/", handleRoot); // @suppress("Ambiguous problem")
	http.on("/device.json", sendDevice); // @suppress("Ambiguous problem")
	http.on("/canal.json", sendCanal); // @suppress("Ambiguous problem")
	http.on("/timerday.json", sendTimerDay); // @suppress("Ambiguous problem")
	http.on("/timerhour.json", sendTimerDay); // @suppress("Ambiguous problem")
	http.on("/timersec.json", sendTimerSec); // @suppress("Ambiguous problem")
	http.on("/timertemp.json", sendTimerTemp); // @suppress("Ambiguous problem")
	http.on("/tempsensor.json", sendTempSensor); // @suppress("Ambiguous problem")
	http.on("/ph.json", sendPhSensor); // @suppress("Ambiguous problem")
	http.on("/tempstats.json", sendTempStats); // @suppress("Ambiguous problem")
//	http.serveStatic("/js", SPIFFS, "/js");
//	http.serveStatic("/css", SPIFFS, "/css");
//	http.serveStatic("/img", SPIFFS, "/img");
//	http.serveStatic("/", SPIFFS, "/index.html");
	http.begin();
}

void sendDevice() {
	if (responseDevice.length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseDevice);
}
void sendCanal() {
	if (responseDevice.length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseCache[0]);
}
void sendTimerDay() {
	if (responseCache[1].length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseCache[1]);
}
void sendTimerHour() {
	if (responseCache[2].length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseCache[2]);
}
void sendTimerSec() {
	if (responseCache[3].length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseCache[3]);
}
void sendTimerTemp() {
	if (responseCache[4].length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseCache[4]);
}
void sendTempSensor() {
	if (responseCache[5].length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseCache[5]);
}
void sendPhSensor() {
	if (responseCachePH.length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseCachePH);
}
void sendTempStats() {
	if (responseCacheTemp.length() == 0) {
		sendNull();
		return;
	}
	http.send(200, "application/json", responseCacheTemp);
}

void sendNull() {
	http.send(200, "application/json", responseNull);
}
boolean OtaDownloadWebContent() {
	SendWiFiLog("HTTP: Downloads web...");
	client.begin(UPDATE_URL + VERTION_PROTOCOL + "update.php");
	int httpCode = client.GET();
	if (httpCode > 0) {
		jsonBuffer.clear();
		JsonObject& root = jsonBuffer.parseObject(client.getString());
		JsonArray& array = root["downloads"].asArray();
		byte attempt = 0;
		for (int i = 0; i < array.size(); i++) {
			String url = array[i]["url"];
			String path = String(array[i]["folder"].asString()) + String(array[i]["file"].asString());
			attempt = 0;

			while (!OtaDownloadFile(url, path)) {
				if (attempt < 3) {
					attempt++;
				} else
					break;

			}
			delay(1000);
		}

	}
}

const int buffSize = 1024;
boolean OtaDownloadFile(String URL, String PathToSave) {

	client.begin(URL);
	int httpCode = client.GET();
	if ((httpCode > 0)) {
		if (httpCode == HTTP_CODE_OK) {
			SendWiFiLog(String(F("HTTP:")) + PathToSave);
			int DownloadSize = client.getSize();
			if (SPIFFS.exists(PathToSave)) {
				File f = SPIFFS.open(PathToSave, "r"); // @suppress("Abstract class cannot be instantiated")
				if (f.size() == DownloadSize) {
					SendWiFiLog(String(F("HTTP: File not change ")));
					client.end();
					f.close();
					return true;
				} else {
					while (SPIFFS.exists(PathToSave)) {
						Serial.print(".");
						SPIFFS.remove(PathToSave);
					}
				}
			}
			int RemainingSize = DownloadSize;
			uint8_t buff[buffSize] = { 0 };
			WiFiClient * stream = client.getStreamPtr();
			unsigned long DebugTimeCycle = millis();

			if (SPIFFS.exists(PathToSave))
				SPIFFS.remove(PathToSave);
			File DownloadFile = SPIFFS.open(PathToSave, "w"); // @suppress("Abstract class cannot be instantiated")
			if (!DownloadFile)
				DownloadFile = SPIFFS.open(PathToSave, "w");
			if (DownloadFile) {

				while (client.connected() && (RemainingSize > 0 || RemainingSize == -1)) {
					auto size = stream->available();
					if (size) {
						auto StreamBytes = stream->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
						DownloadFile.write(buff, StreamBytes);
						DownloadFile.flush();

						if (RemainingSize > 0) {
							RemainingSize -= StreamBytes;
							if ((millis() - DebugTimeCycle >= 1000)) {
								DebugTimeCycle = millis();
								float PercentDownloaded = (((DownloadSize - RemainingSize) * 100) / DownloadSize);
							}
						}
					}
					yield();
				}
				//-----Verify Download  File ------
				if (DownloadFile.size() == DownloadSize) {
					DownloadFile.close();
					client.end();
					return true;
				} else {
					DownloadFile.close();
					client.end();
				}
			} else {
				SendWiFiLog("HTTP:Open File Failed...");
			}
		} else {
			SendWiFiLog("HTTP:Server error...");
		}
	} else {
		SendWiFiLog("HTTP:Server error...");

	}
	client.end();
	return false;

}

String ConvertFormatBytes(int bytes) {
	if (bytes < 1024) {
		return String(bytes) + F("B");
	} else if (bytes < (1024 * 1024)) {
		return String(bytes / 1024.0) + F("KB");
	} else if (bytes < (1024 * 1024 * 1024)) {
		return String(bytes / 1024.0 / 1024.0) + F("MB");
	} else {
		return String(bytes / 1024.0 / 1024.0 / 1024.0) + F("GB");
	}
}

